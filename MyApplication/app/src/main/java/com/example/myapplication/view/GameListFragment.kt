package com.example.myapplication.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.adapter.GameRecyclerAdapter
import com.example.myapplication.viewmodel.GameListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_game_list.*


class GameListFragment : Fragment() {
    private lateinit var viewModel:GameListViewModel
    private val recyclerGameAdapter=GameRecyclerAdapter(arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel= ViewModelProviders.of(this).get(GameListViewModel::class.java)
        viewModel.refreshData()

        gameListRecyclerView.layoutManager= LinearLayoutManager(context)
        gameListRecyclerView.adapter=recyclerGameAdapter

        swipeRefreshLayout.setOnRefreshListener {
            gameLoading.visibility=View.GONE
            gameErrorMessage.visibility=View.VISIBLE
            gameListRecyclerView.visibility=View.GONE
            viewModel.refreshFromInternet()
            swipeRefreshLayout.isRefreshing=false
        }
        observeLiveData()
    }
    fun observeLiveData(){

        viewModel.games.observe(viewLifecycleOwner, Observer {
                games->games?.let {
            gameListRecyclerView.visibility=View.VISIBLE
            recyclerGameAdapter.gameListLive(games)
        }
        })
        viewModel.gameErrorMessage.observe(viewLifecycleOwner, Observer {
                error->error?.let {
                    if(it){
                        gameErrorMessage.visibility=View.VISIBLE
                    } else{
                        gameErrorMessage.visibility=View.GONE
                    }
        }
        })
        viewModel.gameLoading.observe(viewLifecycleOwner, Observer {
                loading->loading?.let {
            if(it){
                gameListRecyclerView.visibility=View.GONE
                gameErrorMessage.visibility=View.GONE
                gameLoading.visibility=View.VISIBLE
            } else{
                gameLoading.visibility=View.GONE
            }
        }
        })

    }

}