package com.example.myapplication.servis

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.myapplication.model.Game

@Dao
interface GameDAO {
    @Insert
    suspend fun insertAll(vararg game: Game):List<Long>

    @Query("SELECT*FROM game")
    suspend fun getAllGame():List<Game>

    @Query("SELECT*FROM game WHERE uuid=:oyunId")
    suspend fun getGame(oyunId:Int):Game

    @Query("DELETE FROM game")
    suspend fun deleteAllGame()
}