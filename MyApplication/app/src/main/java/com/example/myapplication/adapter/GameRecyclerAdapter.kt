package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filterable
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.model.Game
import com.example.myapplication.util.gorselİndir
import com.example.myapplication.util.placeholderYap
import com.example.myapplication.view.GameListFragmentDirections
import kotlinx.android.synthetic.main.fragment_game_details.view.*
import kotlinx.android.synthetic.main.game_recycler_row.view.*
import java.util.*
import java.util.logging.Filter
import kotlin.collections.ArrayList


class GameRecyclerAdapter(val gameList:ArrayList<Game>):RecyclerView.Adapter<GameRecyclerAdapter.GameViewHolder>(){



    class GameViewHolder(itemView:View) :RecyclerView.ViewHolder(itemView){
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val inflater=LayoutInflater.from(parent.context)
        val view=inflater.inflate(R.layout.game_recycler_row,parent,false)
        return GameViewHolder(view)
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        holder.itemView.gameNamee.text=gameList.get(position).name
        holder.itemView.gameDescription.text=gameList.get(position).description

        holder.itemView.setOnClickListener {
            val action=GameListFragmentDirections.actionGameListFragmentToGameDetailsFragment(gameList.get(position).uuid)
            Navigation.findNavController(it).navigate(action)

        }
        holder.itemView.imageView.gorselİndir(gameList.get(position).background_image, placeholderYap(holder.itemView.context))
    }

    override fun getItemCount(): Int {
        return gameList.size
    }
    fun gameListLive(newGameList:List<Game>){
        gameList.clear()
        gameList.addAll(newGameList)
        notifyDataSetChanged()
    }

}