package com.example.myapplication.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.R
import com.example.myapplication.util.gorselİndir
import com.example.myapplication.util.placeholderYap
import com.example.myapplication.viewmodel.GameDetailsViewModel
import kotlinx.android.synthetic.main.fragment_game_details.*
import kotlinx.android.synthetic.main.game_recycler_row.*

class GameDetailsFragment : Fragment() {
    private lateinit var viewModel:GameDetailsViewModel
    private var gameId=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let{
            gameId=GameDetailsFragmentArgs.fromBundle(it).GameId
            println(gameId)
        }
        viewModel= ViewModelProviders.of(this).get((GameDetailsViewModel::class.java))
        viewModel.roomDataGet(gameId)
        observeLiveData()
    }
    fun observeLiveData(){
        viewModel.gameLiveData.observe(viewLifecycleOwner, Observer {
                game->game?.let {
                    gameNamee.text=it.name
                    gameDescription.text=it.description
                    context?.let{
                        gameImage.gorselİndir(game.background_image, placeholderYap(it))
                    }
        }
        })
    }
}

