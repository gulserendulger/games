package com.example.myapplication.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.model.Game
import com.example.myapplication.servis.GameAPIServis
import com.example.myapplication.servis.GameDatabase
import com.example.myapplication.util.OzelSharedPreferences
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch

class GameListViewModel(application: Application):BaseViewModel(application) {
    val games= MutableLiveData<List<Game>>()
    val gameErrorMessage=MutableLiveData<Boolean>()
    val gameLoading=MutableLiveData<Boolean>()

    private val ozelSharedPreferences= OzelSharedPreferences(getApplication())
    private val videoGamesApiServis= GameAPIServis()
    private val disposable= CompositeDisposable()
    private var guncellemeZamani=10*60*1000*1000*1000L

    fun refreshData(){
        val kaydedilmeZamani=ozelSharedPreferences.zamaniAl()
        if(kaydedilmeZamani!=null&&kaydedilmeZamani!=0L&&System.nanoTime()-kaydedilmeZamani<guncellemeZamani){

        }
        else{
            verileriİnternettenAl()
        }

    }
    fun refreshFromInternet(){
        verileriİnternettenAl()
    }

    private fun verileriSQLitetanAl(){
        gameLoading.value=true
        launch {
            val gameList= GameDatabase(getApplication()).gameDao().getAllGame()
            gameGoster(gameList)
            Toast.makeText(getApplication(),"Roomdan aldık", Toast.LENGTH_LONG).show()
        }
    }

    private fun verileriİnternettenAl() {
        gameLoading.value=true
        disposable.add(
            videoGamesApiServis.getData().subscribeOn(Schedulers.newThread()).observeOn(
                AndroidSchedulers.mainThread())
                .subscribeWith(object: DisposableSingleObserver<List<Game>>(){
                    override fun onError(e: Throwable) {
                        gameErrorMessage.value=true
                        gameLoading.value=false
                    }

                    override fun onSuccess(t: List<Game>) {
                        sqliteSakla(t)
                        gameErrorMessage.value=false
                        gameLoading.value=false
                    }

                })
        )

    }

    private fun gameGoster(gameList:List<Game>){
        games.value=gameList

    }

    private fun sqliteSakla(gameList:List<Game>){
        launch {
            val dao=GameDatabase(getApplication()).gameDao()
            dao.deleteAllGame()
            val uuidList =dao.insertAll(*gameList.toTypedArray())

            var i=0
            while (i<gameList.size){
                gameList[i].uuid=uuidList[i].toInt()
                i=i+1
                gameGoster(gameList)
            }

        }
        ozelSharedPreferences.zamaniKaydet(System.nanoTime())
    }
}