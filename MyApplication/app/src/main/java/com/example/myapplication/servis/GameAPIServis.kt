package com.example.myapplication.servis

import com.example.myapplication.model.Game
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class GameAPIServis {

    //https://api.rawg.io/api/
    // games?key=3cb20b795a5e4f38b96cd6527736e8f6
    //3cb20b795a5e4f38b96cd6527736e8f6


    private val BASE_URL="https://api.rawg.io/api/"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(GameAPI::class.java)

    fun getData() : Single<List<Game>> {
        return api.getGame()
    }
}