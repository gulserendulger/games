package com.example.myapplication.servis

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.model.Game

@Database(entities = arrayOf(Game::class),version=1)
abstract class GameDatabase : RoomDatabase() {
    abstract fun gameDao(): GameDAO

    companion object {
        @Volatile private var instance:GameDatabase?=null
        //Volatile diğer threadlere görünür yapmasını sağlıyor.

        private val lock=Any()
        operator fun invoke(context: Context)= instance?: synchronized(lock){
            instance?: databaseOlustur(context).also {
                instance=it
            }
        }


        private fun databaseOlustur(contex: Context) = Room.databaseBuilder(
            contex.applicationContext,
            GameDatabase::class.java,
            "gameDatabase").build()
    }
}