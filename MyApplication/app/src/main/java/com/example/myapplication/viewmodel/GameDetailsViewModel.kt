package com.example.myapplication.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.model.Game
import com.example.myapplication.servis.GameDatabase
import kotlinx.coroutines.launch

class GameDetailsViewModel(application: Application):BaseViewModel(application) {
    val gameLiveData=MutableLiveData<Game>()
    fun roomDataGet(uuid: Int){
        launch {
            val dao= GameDatabase(getApplication()).gameDao()
            val game=dao.getGame(uuid)
            gameLiveData.value=game
        }

    }
}