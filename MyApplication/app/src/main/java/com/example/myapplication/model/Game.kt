package com.example.myapplication.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
@Entity
data class Game(
    @ColumnInfo(name="name")
    @SerializedName("name")
    val name:String?,
    @ColumnInfo(name="id")
    @SerializedName("id")
    val id:Int?,
    @ColumnInfo(name="rating")
    @SerializedName("rating")
    val rating:Int?,
    @ColumnInfo(name="metacritic_rating")
    @SerializedName("metacritic_url")
    val metacritic_rating:String?,
    @ColumnInfo(name="background_image")
    @SerializedName("background_image")
    val background_image:String?,
    @ColumnInfo(name="description")
    @SerializedName("decription")
    val description:String?) {
    @PrimaryKey(autoGenerate = true)
    var uuid:Int=0
}