package com.example.myapplication.servis

import com.example.myapplication.model.Game
import io.reactivex.Single
import retrofit2.http.GET

interface GameAPI {

    //https://api.rawg.io/api/
    // games?key=3cb20b795a5e4f38b96cd6527736e8f6
    //3cb20b795a5e4f38b96cd6527736e8f6

    @GET( "games?key=3cb20b795a5e4f38b96cd6527736e8f6")
    fun getGame(): Single<List<Game>>
}